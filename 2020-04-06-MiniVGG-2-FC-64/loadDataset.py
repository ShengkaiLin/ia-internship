# -*- coding: utf-8 -*-
"""
Created on Tue Mar 17 17:10:34 2020

@author: vandame Paul
"""
#
import numpy as np
import  matplotlib.pyplot as plt
from random import randint

from keras.datasets import cifar10
from keras.utils import to_categorical

#Class Data
#Charge les données et les formates pour l'utilisation en réseau de neurones
#grace à Jiminy

class DataLoaderClass() :
    
    #attribut-------------------
    #x_train X_test = dataset d'entrainement et de test
    #y_train et y_test = label des images
    #dimTrain, dimTest = les dimensions des Dataset avant reshape
    #InputShape = les dimensions a fournir pour l'entrée du réseau
    x_train = None
    y_train = None
    x_test = None
    y_test = None
    
    dimTrain = None
    dimTest = None
    inputShape = None
    #Fonctions de Classes--------

    #constructeur : 
    #récupére les données sur internet et les formates en floats,
    #reshape aussi les label pour du one hot
    #output :
    #self : renvoie la classe elle-même après construction
    #input : 
    #--needed
    #self : la classe elle-même avant construction  
    #le nombre de catégorie de label possible par défautl 10 chiffre [0 9]
    #--not needed
    #num_class = le nombre de class pour ce problème de classification
    #NeuralType = le type de donnée d'entrée, 1 si convolutifs (2 dimensions), 0 si fully-connected
    #DataSetSize = le nombre d'image souhaité au hasard, -1 si on les veut tous
    def __init__(self,num_class=10,neuralType = 0,dataSetSize=-1):
        
        (self.x_train, self.y_train), (self.x_test, self.y_test) = cifar10.load_data()
        
        self.dimTrain = self.x_train.shape
        self.dimTest = self.x_test.shape
        
        #reduce the data set
        if dataSetSize != -1 :
            shuffle = list(np.zeros(dataSetSize))
            for i in range(dataSetSize): shuffle[i]=randint(0,self.dimTrain[0])
            self.x_train = self.x_train[shuffle]
            self.y_train = self.y_train[shuffle]
        if neuralType == 0 :
            self.x_train = self.x_train.reshape(self.x_train.shape[0], self.x_train.shape[1]*self.x_train.shape[2])
            self.x_test = self.x_test.reshape(self.x_test.shape[0], self.x_test.shape[1]*self.x_test.shape[2])
            self.inputShape = int(self.x_train.shape[1])
        else :
            self.x_train = self.x_train.reshape(self.x_train.shape[0], self.x_train.shape[1],self.x_train.shape[2], 3)
            self.x_test = self.x_test.reshape(self.x_test.shape[0], self.x_test.shape[1],self.x_test.shape[2], 3)
            self.inputShape = (self.x_train.shape[1],self.x_train.shape[2],3)#le 3 pour le RGB
        
        self.x_train = self.x_train.astype('float32')
        self.x_test = self.x_test.astype('float32')
        self.x_train /= 255
        self.x_test /= 255
        print('le nombre d element de train est de : '+str(np.size(self.x_train)))
        print('le nombre d element de test est de : '+str(np.size(self.x_test)))
        print('image shape :',self.x_train.shape[1:])
        print(self.x_train.shape[0], 'train samples')
        print(self.x_test.shape[0], 'test samples')
         
        # convert class vectors to binary class matrices
        self.y_train = to_categorical(self.y_train, num_class)
        self.y_test = to_categorical(self.y_test, num_class)   
        print(self.y_test.shape)
    
