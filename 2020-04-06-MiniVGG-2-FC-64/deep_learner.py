#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 17:24:46 2020

@author: shengkailin
"""


# deep learner permets d'entrainer un reseau de neurones 
#dans plusieurs configuration et de créer facilement des plans de tests.
#ils sauvegarde naturellement tout ce que vous faites sur une architecture.
#il permet d'avoir conscience des tests déjà fait et de retracer
#les paramètres choisi. On peut ainsi savoir ce qui a été entrainé et comment.

#Mode de fonctionnement :
#Créer une architecture de réseau de neurones keras contenant un modèle 
#(une architecture). Créer un dataloader,
#Créer une instance de Jiminy avec comme paramètre 
#d'entrée ce modèle et ce dataloader.

import os
import time
from glob import glob
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import tkinter as tk
import csv   
import numpy as np
import ntpath


from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers import RMSprop
from keras.utils.layer_utils import count_params

class DeepLearner():
    # attribut 
    #model =contient le modèle du reseau de neurons
    #OS le type d'OS :windows,linux,mac
    #model_path=address de sauvegarde du model
    #NB_Trainable_parameters=Nombre de parametres a entrainer 
    #cst_pre_train= le nombre de'epochs deja entraine sur le reseau san sauvegard
    model=None
    data=None
    OS=None
    model_path=None
    NB_trainable_parameters=None
    cst_pre_train=None
    
    #fonction de classes------------------------------
    
    #constructeur 
    #creer le model ou le charge en memoire
    #output:
    #self: renvoie la classe elle-meme apres construction
    #input:
    #--needed
    #self: la classe elle-meme avant construction
    #--not needed
    #path:contient le fichier ousauvgarder le reseau
    #DatasetSize:= le nombre d'image souhaite dans le train,-1 si on les veut tous
    def __init__(self,data,architecture=None,OS="windos",model_path=None):
        
        #initialise les atributs de la class 
        self.model_path=model_path
        self.OS=OS
        self.cst_pre_train=0
        self.data=data
        #detruit les eventulles fichier temporaire restant
        files=glob(os.getcwd()+"/Cache_DeepLearner/*")
        for file in files: os.remove(file)
        #cherche le model a entrainer---------------------------------
        if self.model_path==None:
            #charger une architecture
            self.model=architecture.model
        else:
            #charger une architecture pres entraine
            self.model=load_model(self.model_path)
            
        self.model.summary()
        self.NB_trainnable_parameters=count_params(self.model.trainable_weights)
        
    #detruit les fichier temporaire avant destruction
    def __delet__(self): 
        
        files=glob(os.getcwd()+"/Cache_DeepLearner/*")
        for file in files: os.remove(file) 
        
    #compile le modele keras pour permeterre ensuite un entrainement sur CPU/GPU 
    def Compile(self):
      self.model.compile(loss="binary_crossentropy",optimizer=RMSprop(),metrics=["accuracy"])
      print("problme classifeur detecte...")
      
      
    def Train(self,batch_size=1000,nb_epochs=100,validation_freq=10,node_FC=128):
        #training--------------------
        #nb_epochs/validation_freq doit etre entie
        assert nb_epochs/validation_freq==int(nb_epochs/validation_freq)
        
        training_history=self.model.fit(
            self.data.x_train,self.data.y_train,batch_size=batch_size,verbose=1,
            epochs=nb_epochs,validation_freq=validation_freq,
            validation_data=(self.data.x_test,self.data.y_test))
        
       #test model and save 
        res=self.TestModel()
        print("\n"+"taux de mauvaise prediction:"+str(res)+"\n")
        self.PopUpSave(batch_size,nb_epochs,training_history.history["loss"][-1],
                       training_history.history["val_loss"][-1],training_history,
                       validation_freq,node_FC)
      
       #Propose une sauvegarde des modeles en temps reel
       #OutPut:None
       #Input:
       #BATCH_SIZE,nb_epoch,losstest,losstrain,history,validation_freq
    def PopUpSave(self,batch_size,nb_epochs,losstrain,losstest,history,validation_freq,node_FC):
        #plot history pour comprendre les loss et juger de la suavegarde
        #detruit les anciens affichage
        plt.close("all")
        time.sleep(1)
        #trace les courbe d'apprentissage
        plt.style.use("ggplot")
        plt.figure()
        plt.plot(np.arange(0,nb_epochs),history.history["loss"],label="Train_loss")
        plt.plot(np.arange(0,nb_epochs),history.history["val_loss"],label="Test_loss")
        plt.xlabel("epoch#")
        plt.xlabel("loss")
        plt.title("mauvaise_prediction="+str(self.TestModel())+"batch_size="+str(batch_size)+
                                             "node_FC="+str(node_FC))
        plt.legend()
        time.sleep(1)
        #sauvegarder le graph
        name=str(len(os.listdir("./save/loss_graph"))+1)+"_from"+str(self.model_path)+"_batch_size=" \
        +str(batch_size)+"_epoch="+str(nb_epochs)+"_lossTrain="+str(round(losstrain,3)) \
        +"_lossTest="+str(round(losstest,3))+"_FpredictionRate_"+str(self.TestModel())
        
        print("name of the plot:"+name)
        plt.savefig("save/loss_graph/"+name+".png")
        #ferme tous les courbes
        plt.close("all")
        time.sleep(1)
        
        #sauvegarde les courbes de loss en csv pour une utilisation exterieur 
        
        with open("save/loss_array/train.csv","a") as csvfile:
            spamwriter=csv.writer(csvfile,lineterminator="\n")
            spamwriter.writerow(list(history.history["loss"]))
        with open("save/loss_array/test.csv","a") as csvfile:
            spamwriter=csv.writer(csvfile,lineterminator="\n")
            spamwriter.writerow(list(history.history["val_loss"]))

        #sauve garde le paln d'experience en plot et en numpy(numpy)
           
        plan = None
        figLossTrainBatchEpoch = plt.figure()
        figLossTestBatchEpoch = plt.figure()
        figLossTrainWeightLayer = plt.figure()
        figLossTestWeightLayer = plt.figure()
        figLossTestWeightData = plt.figure()
            
        axTrainBatchEpoch = figLossTrainBatchEpoch.gca(projection='3d')
        axTestBatchEpoch = figLossTestBatchEpoch.gca(projection='3d')
        axLossTrainWeightLayer = figLossTrainWeightLayer.gca(projection='3d')
        axLossTestWeightLayer = figLossTestWeightLayer.gca(projection='3d')            
        axLossTestWeightData = figLossTestWeightData.gca(projection='3d')
            
        #récupére les ancien entrainement et 
        #test si ce réseau à déjà été appris quelque par 
        #pour mettre le numéro d'epoch totale à jour
        if os.path.exists('save/plan_EXP/plan.npy') :
            plan = list(np.load('save/plan_EXP/plan.npy',allow_pickle=True))
            if self.model_path != None :
                self.cst_pre_train = plan[int(self.model_path)-1][0]
                    
        else:
            plan = list()
                
        plan.append([nb_epochs,batch_size,history.history['loss'],
                        history.history['val_loss'],self.cst_pre_train,
                        self.NB_trainnable_parameters,len(self.model.layers),self.data.x_train.shape[0]])
        np.save('save/plan_EXP/plan.npy',plan,allow_pickle=True)
             
        #Créer les png concernant les plan 
        #loss(epoch,batchsize) pour train et test
        #loss(layer,weight) pour train et test
        for i in range(0,len(plan)): 
            axTrainBatchEpoch.scatter3D(np.linspace(1+int(plan[i][4]),int(plan[i][0])+int(plan[i][4]),int(plan[i][0])),
                                  int(plan[i][1])*np.ones(int(plan[i][0])),plan[i][2],cmap='Greens',label='model '+str(i))
            axTestBatchEpoch.scatter3D(np.linspace(1+int(plan[i][4]),int(plan[i][0])+int(plan[i][4]),int(len(plan[i][3]))),
                                 int(plan[i][1])*np.ones(int(len(plan[i][3]))),plan[i][3],cmap='Greens',label='model '+str(i))                
            axLossTrainWeightLayer.scatter3D(plan[i][6],plan[i][5],plan[i][2][-1],cmap='Greens',label='model '+str(i))
            axLossTestWeightLayer.scatter3D(plan[i][6],plan[i][5],plan[i][3][-1],cmap='Greens',label='model '+str(i))
            axLossTestWeightData.scatter3D(plan[i][7],plan[i][5],plan[i][3][-1],cmap='Greens',label='model '+str(i))
        #set les labels et légendes            
        axTrainBatchEpoch.set_xlabel('nb_epochs')
        axTrainBatchEpoch.set_ylabel('batch_size')
        axTrainBatchEpoch.set_zlabel('Train loss')
        axTrainBatchEpoch.legend()
        axTestBatchEpoch.set_xlabel('nb_epochs')
        axTestBatchEpoch.set_ylabel('batch_size')
        axTestBatchEpoch.set_zlabel('Train loss')
        axTestBatchEpoch.legend()
        axLossTrainWeightLayer.set_xlabel('NB layer')
        axLossTrainWeightLayer.set_ylabel('NB trainnable weight')
        axLossTrainWeightLayer.set_zlabel('Train Loss')
        axLossTrainWeightLayer.legend()
        axLossTestWeightLayer.set_xlabel('NB layer')
        axLossTestWeightLayer.set_ylabel('NB trainnable weight')
        axLossTestWeightLayer.set_zlabel('Test Loss')             
        axLossTestWeightLayer.legend()
        axLossTestWeightData.set_xlabel('NB Data')
        axLossTestWeightData.set_ylabel('NB trainnable weight')
        axLossTestWeightData.set_zlabel('Train Loss')
        axLossTestWeightData.legend()
        #Créer les png  en fonction des figures
        figLossTrainBatchEpoch.savefig('save/plan_EXP/Plan_Train_loss_batch_epochs.png')
        figLossTestBatchEpoch.savefig('save/plan_EXP/Plan_Test_loss_batch_epochs.png')
        figLossTrainWeightLayer.savefig('save/plan_EXP/Plan_Train_loss_Weight_Layer.png')
        figLossTestWeightLayer.savefig('save/plan_EXP/Plan_Test_loss_Weight_Layer.png')   
        figLossTestWeightData.savefig('save/plan_EXP/Plan_Test_loss_Weight_Data.png')   
        #donne un nom au programme en cas de réentrainement après sauvegarde,
        #le fichier fera le lien entre les 2 programmes
        self.model_path = len(os.listdir('./save/model'))
        
        #demande l'utilisateur s'il veut sauvegarder le model(.h)
        root=tk.Tk()
        root.attributes("-topmost",True)
        root.withdraw()
        MsgBox=tk.messagebox.askquestion("save","sauverle model")
        if MsgBox=="yes":
            self.model.save("save/model/"+name+".h5")
        
        plt.close("all")
        time.sleep(1)
        root.destroy()
        
        #metre a jour le nombre d'epochs
        self.cst_pre_train=nb_epochs
        
    #detruit toutes les sauvegardes pre-enregistre dans save
    def Clearsave():
        files=glob(os.getcwd()+"/save/**/*")
        for file in files:os.remove(file)
        
        #test the prediciton of the model to see the result with an human comprehension
        #Input : No input
        #Output : le ratio d'erreur ou l'erreur 
        #quadratique moyenne en fonction de si le problème est regressif ou 
        #classificateur. Un pb regressif est supposé pour des image (4D)  
    def TestModel(self):
        if len(self.data.y_test.shape)>2 :
            yhat=self.model.predict(self.data.x_test)
            ratioErreur = ((yhat-self.data.y_test)**2).mean()
        else :
            yhat=self.model.predict(self.data.x_test)
            res = 0
            for i in range(yhat.shape[0]) : res+=int( int(np.argmax(yhat[i][:]))!= int(np.argmax(self.data.y_test[i][:])))
            ratioErreur = res/yhat.shape[0]
        return ratioErreur 


    @staticmethod
    def path_leaf(path):
         head, tail = ntpath.split(path)
         return tail or ntpath.basename(head)
                    
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    