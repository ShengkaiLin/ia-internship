# -*- coding: utf-8 -*-
#import lib 
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D,MaxPooling2D,Flatten,Dense
from tensorflow.keras import backend as K


class LeNet:
    
    def build(width,height,depth,classes):
        """
    
        Parameters
        ----------
        width : int
            input size as an RGB image ,first dimension
        height : input 
            size as an RGBimage second dimension
        depth : int
          size as an RGBimage third dimension
        classes : int
            number of classes in the dataset

        Returns
        -------
        LeNet 

        """
        model=Sequential()
        inputShape=(height,width,depth)
        
        #if using "channels first"
        if K.image_data_format()=="channels_first":
            inputShape=(depth,height,width)
         
        #first set of conv=>RELU=>POOL layers
        model.add(Conv2D(20,(5,5),padding="same",input_shape=inputShape,activation="relu"))
        model.add(MaxPooling2D(pool_size=(2,2),strides=(2,2)))
        
        #second set of CONV=>RELU=>POOL
        model.add(Conv2D(50,(5,5),padding="same",activation="relu"))
        model.add(MaxPooling2D(pool_size=(2,2),strides=(2,2)))
        
        #first set of FC=>RELU
        model.add(Flatten())
        model.add(Dense(500,activation="relu"))
        
        #softmax classifier
        model.add(Dense(classes,activation="softmax"))
        
        #model summary
        model.summary()
        
        return model
    
    
    
if __name__=="__main__":
 model=LeNet.build(width=28,height=28,depth=1,classes=10)         
        
        
       
    

