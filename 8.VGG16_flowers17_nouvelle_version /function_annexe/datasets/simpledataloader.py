# -*- coding: utf-8 -*-
#usage python simpledataloader.py --dataset ../../../datasets/animals
#import the necessary package 
import numpy as  np
from imutils import paths
import cv2
import os
import sys
import argparse
#sys.path.append("../preprocessing")
#from imgetoarraypreprocessor import ImageToArrayPreprocessor
#from simpleporeprocessor import SimplePreprocessor

class SimpleDatasetLoader():
    def __init__(self,preprocessors=None):
        #store the preprocessor
        self.preprocessors=preprocessors
        
        #if preprocessors is empty
        if self.preprocessors is None :
           self.preprocessors=[]
        
    def load(self,imagePaths,verbose=-1):
        """
        

        Parameters
        ----------
        imagePaths : list 
           list des paths de tous les images
        verbose : int
            DESCRIPTION. The default is -1.

        Returns
        -------
        np array images preprocés et leur labels

        """
        #initialize data et label
        data=[]
        labels=[]
        
        #loop overs input imagePaths
        for (i,imagepath) in enumerate(imagePaths):
             # suppose que le path est configure comme:
             #/path/to/dataset/{class}/{image}.png
             image=cv2.imread(imagepath)
             label=imagepath.split(os.path.sep)[-2]
         
             #si le preprocessor est defini
             if self.preprocessors is not None:
             
                for p in self.preprocessors:
                     image=p.preprocess(image)
             #ajouter dans la list 
             data.append(image)
             labels.append(label)    
             #show update chaque 'verbose' images
             if verbose>0 and i>0 and (i+1)%verbose==0 :
                print("[INFO] processed{}/{}.".format(i+1,len(imagePaths)))
            
        return(np.array(data),np.array(labels))


if __name__=="__main__":
     ap=argparse.ArgumentParser()
     ap.add_argument("-d","--dataset",required=True, help="path to input dataset")
     args=vars(ap.parse_args())
     iap=ImageToArrayPreprocessor()
     sp=SimplePreprocessor(128,128)
     imagePaths=list(paths.list_images(args["dataset"]))
     sdl=SimpleDatasetLoader(preprocessors=sp)
    
     
                 
        