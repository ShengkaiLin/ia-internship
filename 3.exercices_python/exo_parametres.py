# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 15:07:25 2020

@author: lshengkai
"""

def affichage0():
    print("bonjour ")

def affichage1(value1,value2):
    print("bonjour ", value1,value2)

def affichage2(value1='toto',value2='titi'):
    print("bonjour ", value1,value2)

def affichage3(value1='toto',value2='titi'):
    print("bonjour ", value1,value2)
    
affichage0()
# affichage1() Celui la plantera car les parameteres n'ont pas de valeurs par defaut
affichage1('phillipe', 'shengkai')
affichage2()
affichage2('phillipe','shengkai')
affichage2(value2='phillipe',value1='shengkai')

