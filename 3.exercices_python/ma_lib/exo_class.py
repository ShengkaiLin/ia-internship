# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 15:25:27 2020

@author: lshengkai
"""

class Animal():
    def __init__(self, aFamily="Inconnu"):
        """
        Constructeur
        
        Ceci est un simple constructeur
        """
        self.mName = "Animal"
        self.mFamily = aFamily
    
    def AfficherMonNom(self):
        """
        Affiche un nom
        
        Cette fonction doit me permettre...
        """
        print(self.mName)

    def __repr__(self):
        """
        Affiche sa représentation en string mais pour developpeur
        
        Cette fonction doit me permettre...
        """
        return self.mName+" / "+self.mFamily+" / "
        
    def __str__(self):
        """
        Affiche sa représentation en string
        
        Cette fonction doit me permettre...
        """
        return self.mName+" de la famille "+self.mFamily
 
class Loup(Animal):
    def __init__(self):
        super(Loup,self).__init__()
        self.mName = "Loup"
    
    def AfficherMonNom(self):
        """
        Affiche un nom
        
        Cette fonction doit me permettre...
        """
        print(self.mFamily)
        super(Loup,self).AfficherMonNom()

if __name__ == "__main__":    
    #vAnimalExample = Animal(aFamily="Autre famille")
    vAnimalExample = Loup()
    vAnimalExample.AfficherMonNom()
    help(Animal.AfficherMonNom)
    
    print(str(vAnimalExample))
    print(repr(vAnimalExample))
