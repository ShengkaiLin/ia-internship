# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 15:42:44 2020

@author: lshengkai
"""
#usage main.py --image_index 2

#%% load le model de miniVGG3-add-conv meilluer resultat 
#import lib 
from tensorflow.keras.models import load_model
from tensorflow.keras.utils import plot_model
model=load_model("6_fromNone_batch_size=100_epoch=50_lossTrain=0.021_lossTest=0.336_FpredictionRate_0.2477.h5")
model.summary()
plot_model(model,show_shapes=True)
#%% preprocessing  10 image examplaire form cifar 10
from keras.preprocessing import image
from imutils import paths
import cv2
import numpy as np
#imagePaths="cifar_10_examplaire/training_data"
imagePaths="cifar10_examplaire/validation_data"
imagePaths=list(paths.list_images(imagePaths))
Image=np.empty([10,32,32,3])
for (i,imagePath)in enumerate(imagePaths):
    image_=cv2.imread(imagePath)
    image_=(cv2.resize(image_,(32,32)))
    image_tensor=image.img_to_array(image_)
    image_tensor=np.expand_dims(image_tensor,axis=0)
    image_tensor/=255.
    Image[i]=image_tensor

print(Image[4].shape)
 
#%%  display an image de training_data
import matplotlib.pyplot as plt
import argparse
ap = argparse.ArgumentParser()
ap.add_argument("-ind", "--image_index", required = True,type=int,
    help = "int from 1 to 10")
args = vars(ap.parse_args())
#voir un image de chat de training_data
plt.imshow(Image[args["image_index"]])
plt.show()  

#%% instantialise un model qui contients les sorties de tous les couches intermidiares  
from tensorflow.keras import models
# le chiffre 9 signifie la derniere couche max_pooling 
layer_outputs=[layer.output for layer in model.layers[:9]]
activation_model=models.Model(inputs=model.input,outputs=layer_outputs)

#%% faire une prediction
activations=activation_model.predict(np.expand_dims(Image[args["image_index"]],axis=0))
first_layer_activation=activations[0]
print(first_layer_activation.shape)

#%% visulise un channel
import matplotlib.pyplot as plt
plt.matshow(first_layer_activation[0,:,:,10],cmap="viridis")

#%% visualize chaque channel dans tous les couches 
layer_names=[]
for layer in model.layers[:9]:
    layer_names.append(layer.name)
images_per_row=16
i=0
for layer_name,layer_activation in zip(layer_names,activations):
  i+=1
  n_features=layer_activation.shape[-1]
  size=layer_activation.shape[1]
  n_cols=n_features// images_per_row
  display_grid=np.zeros((size*n_cols,images_per_row*size))
  
  for col in range(n_cols):
      for row in range(images_per_row):
          channel_image=layer_activation[0,:,:,col*images_per_row+row]
          display_grid[col*size:(col+1)*size,row*size:(row+1)*size]=channel_image
  scale=1./size
  plt.figure(figsize=(scale*display_grid.shape[1],scale*display_grid.shape[0]))
  plt.title(layer_name)
  plt.grid(False)
  plt.imshow(display_grid,aspect="auto",cmap="viridis")
  save_dir="DeepFeature/validation_dataset/xxx/"
  plt.savefig(save_dir+"sortie_{}".format(i))    
#%% visualize l'image moyenne dans chaque couches
layer_names=[]
for layer in model.layers[:9]:
    layer_names.append(layer.name)
i=0
for layer_name,layer_activation in zip(layer_names,activations):
   i+=1
   n_features=layer_activation.shape[-1]
   size=layer_activation.shape[1]
   channel_image=np.mean(layer_activation[0],axis=-1)
   plt.figure()
   plt.title(layer_name+"moyenné")
   plt.grid(False)
   save_dir="DeepFeature/validation_dataset/xxx/"
   plt.imshow(channel_image,aspect="auto",cmap="viridis")
   plt.savefig(save_dir+"sortie_moyenne_{}".format(i))
   plt.colorbar()