# -*- coding: utf-8 -*-
"""
Created on Mon Apr 06 10:41:04 2020

@author: Lin Shengkai
"""
#import lib
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D,BatchNormalization,MaxPooling2D,Flatten,Dropout,Dense



class MiniVGGNet():
    
        #Attribut
    #model = l'architecture a fournir 
    model=None
    #Fonctions de Classes--------

    #Créateur de l'architecture
    #InputDim = La shape d'entrée
    def __init__(self,width,height,depth,classes,node_FC):
        
        #initialize input shape
        self.model=Sequential()
        inputShape=(height,width,depth)
        
        
        #frist CONV=>RELU=>CONV=>RELU=>POOL layer set 
        self.model.add(Conv2D(32,(3,3),padding="same",input_shape=inputShape,
                             activation="relu"))
        #self.model.add(BatchNormalization(axis=chanDim))
        self.model.add(Conv2D(32,(3,3),padding="same",activation="relu"))
        #self.model.add(BatchNormalization(axis=chanDim))
        self.model.add(MaxPooling2D(pool_size=(2,2)))
        #self.model.add(Dropout(0.25))
        
        #second CONV=>RELU=>CONV=>RELU=>POOL layer set 
        self.model.add(Conv2D(64,(3,3),padding="same",input_shape=inputShape,
                             activation="relu"))
        #self.model.add(BatchNormalization(axis=chanDim))
        self.model.add(Conv2D(64,(3,3),padding="same",activation="relu"))
        #self.model.add(BatchNormalization(axis=chanDim))
        self.model.add(MaxPooling2D(pool_size=(2,2)))
        #self.model.add(Dropout(0.25))
        
        #third CONV=>RELU=>CONV=>RELU=>POOL
        self.model.add(Conv2D(128,(3,3),padding="same",input_shape=inputShape,
                             activation="relu"))
        #self.model.add(BatchNormalization(axis=chanDim))
        self.model.add(Conv2D(128,(3,3),padding="same",activation="relu"))
        #self.model.add(BatchNormalization(axis=chanDim))
        self.model.add(MaxPooling2D(pool_size=(2,2)))
        #self.model.add(Dropout(0.25))
        
        #only layer of FC=>RELU layers
        self.model.add(Flatten())
        self.model.add(Dense(node_FC,activation="relu"))
        #self.model.add(BatchNormalization())
        #self.model.add(Dropout(0.5))
        
        #softmax classifier 
        self.model.add(Dense(classes,activation="softmax"))
        