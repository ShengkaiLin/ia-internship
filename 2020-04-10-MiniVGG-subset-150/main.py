# -*- coding: utf-8 -*-
#usage exemplaire: python main.py --batch_size 1000 --nb_epochs 50  --node_FC 512


#import les libs necessaires

import argparse 
#import ma lib 



from loadDataset import DataLoaderClass
from deep_learner import DeepLearner
from minivggnet import MiniVGGNet


# construct the argument parse and parse the arguments(start_layer,batch_size,nb_epochs etc)

ap = argparse.ArgumentParser()
ap.add_argument("-b", "--batch_size", type=int,default=100,
	help="# la taille de batch dataset")
ap.add_argument("-n", "--nb_epochs", type=int, default=50,
	help="# le nombre de epochs lors d'un apprentissage")
ap.add_argument("-nd","--node_FC",type=int,default=128,
                help="# le nombre de neurons dans le couche FC")


args = vars(ap.parse_args())

#verifier tous les parametres d'entrees
print(args)

#load data 
print("[info] loading cifar-10 data...")
data = DataLoaderClass(dataSetSize=150,neuralType=1)

#load model reconfiguree une image de cifar10(10 classes en total) procede une taille de 32*32*3
print("[info] build model...")
model=MiniVGGNet(width=32,height=32,depth=3,classes=10,node_FC=args["node_FC"])

#using Jiminy pour compiler
DL= DeepLearner(data,model)
DL.Compile()
input('appuyer sur enter pour continuer (pas Ctr+C ): ')
DL.Train(batch_size=args["batch_size"],nb_epochs=args["nb_epochs"],validation_freq=1,node_FC=args["node_FC"])



