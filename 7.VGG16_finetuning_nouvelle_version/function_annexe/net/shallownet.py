# -*- coding: utf-8 -*-
#import libs 
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D,Flatten, Dense 
from tensorflow.keras import backend as K

class ShallowNet:
    def build(width,height,depth,classes):
        """
        Parameters
        ----------
        width : int
            premiere dimension d'une image '
        height : int
            deuxime dimension dune image
        depth : int
            troisieme dimension dune image(channel)
        classes : int
            nombre de class

        Returns
        -------
        CNN sequentielle avec une entee taille precise

        """
        model=Sequential()
        inputShape=(height,width,depth)
    
        if K.image_data_format()=="channels_first":
            inputShape=(depth,height,width)
            
        #define les couchs 
        model.add(Conv2D(32,(3,3),padding="same",input_shape=inputShape,activation="relu"))
        model.add(Flatten())
        model.add(Dense(classes,activation="softmax"))
            
        return model 
            

