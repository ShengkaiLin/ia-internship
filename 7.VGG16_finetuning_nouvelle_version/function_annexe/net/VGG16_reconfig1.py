# -*- coding: utf-8 -*-
"""
class vgg16_reconfig
Created on Mon Mar 16 13:22:47 2020
@author: lshengkai 
"""

#import lib
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.layers import Flatten,Dense,Dropout
from tensorflow.keras.models import Sequential
import numpy as np
from sklearn.preprocessing import LabelBinarizer
class VGG16_reconfig:
    def build(width,height,depth,classes,start_layer,nb_fc_layer):
        """
        Parameters
        ----------
        width : premier dimension d'une tensor d'image
            
        height : deuxieme dimension d'une tensor d'image
           
        depth : troisime dimension d'une tensor d'image
            
        classes : nombres de class de dataset
            
        start_layers : a partir de ce couche ,on met tous les couches trainables
            

        Returns
        -------
        un models dont la sortie a taille "classes"  
        """
        model=VGG16(include_top=False,weights="imagenet",input_shape=(width,height,depth))
        model.summary() 
        
        for layer in model.layers[:start_layer]:
            layer.trainable=False
        for layer in model.layers[start_layer:]:
            layer.trainable=True
       
                
        
        model1=Sequential()
        model1.add(model)
        model1.add(Flatten())
        model1.add(Dense(nb_fc_layer,activation="relu"))
        model1.add(Dropout(0.5))
        model1.add(Dense(classes,activation="softmax"))
        return model1
    
def step_decay(epoch):
    
    #initializing the base initial learning rate, drop factor and epochs to drop every
    initAlpha=0.01
    factor=0.25
    dropEvery=5
    
    #compute learning ate for the currenct epoch
    alpha=initAlpha*(factor**np.floor((1+epoch)/dropEvery))
    
    #return the learning rate
    return float(alpha)    

def preprocessing(x_train,y_train,x_test,y_test):
    """

    Parameters
    ----------
    x_train : unit
        cifar10 d'apprentissage 50000*32*32*3
    y_train : int
        label cifar10 d'apprentissage 
    x_test : unit
        cifar10 validatio data:10000*32*32*3
    y_test : int
        label cifar10 validation 

    Returns
    -------
    les data sont downscale [0,1] et convert les label de int vers matrix

    """
 
    #scale les data vers [0,1] et convert label de int vers vect       
    x_train=x_train.astype("float")/255.0 
    x_test=x_test.astype("float")/255.0
    lb=LabelBinarizer()
    y_train=lb.fit_transform(y_train)
    y_test=lb.transform(y_test)
        
    return x_train,y_train,x_test,y_test


if __name__=="__main__":
    
 model=VGG16_reconfig.build(32,32,3,10,170)
 model.summary()

