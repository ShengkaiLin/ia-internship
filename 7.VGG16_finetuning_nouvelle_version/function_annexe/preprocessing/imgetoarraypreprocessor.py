# -*- coding: utf-8 -*-
#import the necessary packages
from tensorflow.keras.preprocessing.image import img_to_array
import cv2
class ImageToArrayPreprocessor:

    def __init__(self,dataFormat=None):
        """
        

        Parameters
        ----------
        dataFormat : pour keras qui utilise tensorflow backend ,ce parameters est 
    channels_last,les images sont configures comme(rows,columns,channels)              
    (32,32,3) par exemple
            DESCRIPTION:Initialization

        Returns
        -------
        None.

        """
        #store the image data format
        self.dataFormat=dataFormat

    def preprocess(self,image):
         """
         

         Parameters
         ----------
         image : input 
             DESCRIPTION.

         Returns 
         -------
         a NumPy array with channels properly ordered

         """
         return img_to_array(image,data_format=self.dataFormat)
     
#run the script 
if __name__=="__main__":
    iap=ImageToArrayPreprocessor()
    image=cv2.imread("/Users/shengkailin/project_intership_2020/datasets/animals/dogs/dogs_00001.jpg")
    print(iap.dataFormat)
    image2=iap.preprocess(image)
