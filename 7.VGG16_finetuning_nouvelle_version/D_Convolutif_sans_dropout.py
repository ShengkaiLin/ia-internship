# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 19:41:04 2020

@author: Paul Vandame
"""
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Conv2D,MaxPooling2D,Flatten,Dense


#Decrit une architecture de réseau de neurones simple  cnn pour classifier
class VGG16_reconfig():
    
    #Attribut
    #model = l'architecture a fournir    
    model = None
    
    #Fonctions de Classes--------

    #Créateur de l'architecture
    #InputDim = La shape d'entrée
    def __init__(self,width,height,depth,classes,start_layer,nb_fc_layer):
        """
        Parameters
        ----------
        width : premier dimension d'une tensor d'image
            
        height : deuxieme dimension d'une tensor d'image
           
        depth : troisime dimension d'une tensor d'image
            
        classes : nombres de class de dataset
            
        start_layers : a partir de ce couche ,on met tous les couches trainables
            

        Returns
        -------
        un models dont la sortie a taille "classes"  
        """
        model2=VGG16(include_top=False,weights="imagenet",input_shape=(width,height,depth))
        model2.summary() 
        
        for layer in model2.layers[:start_layer]:
            layer.trainable=False
        for layer in model2.layers[start_layer:]:
            layer.trainable=True
       
                
        
        self.model=Sequential()
        self.model.add(model2)
        self.model.add(Flatten())
        self.model.add(Dense(nb_fc_layer,activation="relu"))
        self.model.add(Dense(classes,activation="softmax"))
        
        
