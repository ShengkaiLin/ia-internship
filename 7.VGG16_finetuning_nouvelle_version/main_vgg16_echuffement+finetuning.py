#usage examplaire 
#finetune_flowers17.py --dataset dataset --model model

import os
import argparse
import numpy as np
import matplotlib.pyplot as plt
from imutils import paths
from keras.models import Model
from keras.layers import Input
from keras.applications import VGG16
from keras.optimizers import SGD
from keras.optimizers import RMSprop
from function_annexe.net.FCHeadNet import FCHeadNet

from sklearn.metrics import classification_report

#import les libs necessaires
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.optimizers import SGD
import argparse 
#import ma lib 
from function_annexe.net.VGG16_reconfig1 import preprocessing
from function_annexe.callbacks.trainmonitor import TrainingMonitor
import os


# grab the lisr of iamges that we'll be describing, then extract
# the class label names rfrom the image paths
# assume that our input dataset has the following directory structure:
#dataset_name/{class_name}/example.jpg
print("[INFO] loading iamges...")
(x_train,y_train),(x_test,y_test)=cifar10.load_data()
classNames={"airplane","automobile","bird","cat","deer","dog","frog","horse","ship","truck"}

#scale les data vers [0,1] et convert label de int vers vect
x_train,y_train,x_test,y_test=preprocessing(x_train,y_train,x_test,y_test)



# performing network surgery
# load the VGG16 network, ensuring the head FC layer sets are left off
baseModel = VGG16(weights = "imagenet", 
                include_top = False,
                input_tensor = Input(shape = (32, 32, 3))
    )


# initialize the new head of the network, a set of FC layers
# followed by a softmax classifier
headModel = FCHeadNet.build(baseModel, len(classNames), 256)


# place the head FC model on top of the base model -- this will
# become the actual model we will train
model = Model(inputs = baseModel.input, outputs = headModel)


# loop over all layers in the base model
# and freeze them so they will *not* be updated during the training process
for layer in baseModel.layers:
    layer.trainable = False


# compile ur model
print("[INFO] compiling model...")
optimizer = RMSprop(lr = 0.001)
model.compile(loss = "categorical_crossentropy",
            optimizer = optimizer,
            metrics = ["accuracy"]
    )


# train the head of the network for a few epochs (all other layers are frozen)
# -- this will allow the new FC layers to start to become initialized with actual
# *learned* values versus pure random
print("[INFO] training head...")
H1=model.fit(x_train,y_train, batch_size = 100,
                validation_data = (x_test, y_test),
                epochs = 20,
            
                verbose = 1
    )


# evaluate the network on the fine-tuned model
print("[INFO] evaluating after fine-tuning(echauffement)...")
predictions = model.predict(x_test, batch_size = 100)
print(classification_report(y_test.argmax(axis = 1), predictions.argmax(axis = 1),
                                                    target_names = classNames))


#plot 
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, 20), H1.history["loss"], label="train_loss")
plt.plot(np.arange(0, 20), H1.history["val_loss"], label="val_loss")
plt.title("Training&validation loss__echauffement")
plt.xlabel("Epoch #")
plt.ylabel("Loss")
plt.legend()



#now that the head FC layers have been trained/initialized,lets 
#unrezze the final set of CONV layers and make them trainable
for layer in baseModel.layers[17:]:
    layer.trainable=True
    
#for the changes to the model to take affect we need to recomlile 
#the model, this time using GSD with a very small learning rate 
print("[INFO] re-compling model...")
opt=RMSprop(lr=2e-5)
model.compile(loss="categorical_crossentropy",optimizer=opt,metrics=["accuracy"]) 


#see how many parameters are trainables
model.summary()

#train the model again, this time fine-tuning *both* the final set 
#of CONV layers along with our set of FC layers
print("[INFO] fube-tuning model...") 
H2=model.fit(x_train, y_train, batch_size = 100,
                validation_data = (x_test, y_test),
                epochs = 100,
                
                verbose = 1
    ) 
#evaluate the network on the fine-tuned model
print("[INFO] evaluating after fine-tuning...")
predictions=model.predict(x_test,batch_size=100)
print(classification_report(y_test.argmax(axis=1),predictions.argmax(axis=1),
                            target_names=classNames)) 

#plot 
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, 100), H2.history["loss"], label="train_loss")
plt.plot(np.arange(0, 100), H2.history["val_loss"], label="val_loss")
plt.title("Training&validation loss_finetuning")
plt.xlabel("Epoch #")
plt.ylabel("Loss")
plt.legend()

   
