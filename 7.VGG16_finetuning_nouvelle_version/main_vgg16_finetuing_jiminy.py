# -*- coding: utf-8 -*-
#usage exemplaire: python main.py --start_layer -4 --batch_size 1000 --nb_epochs 50  
#--nb_fc_layer 256 

#import les libs necessaires
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.optimizers import SGD
import argparse 
#import ma lib 


from function_annexe.callbacks.trainmonitor import TrainingMonitor
from A_loadDataset import DataLoaderClass
from B_Jiminy import Jiminy
from D_Convolutif import VGG16_reconfig
import os

# construct the argument parse and parse the arguments(start_layer,batch_size,nb_epochs etc)
def boolean_string(s):
    if s not in {'False', 'True'}:
        raise ValueError('Not a valid boolean string')
    return s == 'True'

ap = argparse.ArgumentParser()
ap.add_argument("-s", "--start_layer", type=int,default=-4,
	help="le premier couche a partir duquel sont trainables")
ap.add_argument("-b", "--batch_size", type=int,default=100,
	help="# la taille de batch dataset")
ap.add_argument("-n", "--nb_epochs", type=int, default=50,
	help="# le nombre de epochs lors d'un apprentissage")

ap.add_argument("-nb_fc","--nb_fc_layer",type=int,default=1024,required=True,
                help="the number of the ")

args = vars(ap.parse_args())

#verifier tous les parametres d'entrees
print(args)

#load data 
print("[info] loading cifar-10 data...")
data = DataLoaderClass(dataSetSize=-1,neuralType=1)

#load model reconfiguree une image de cifar10(10 classes en total) procede une taille de 32*32*3
print("[info] build model...")
model=VGG16_reconfig(width=32,height=32,depth=3,classes=10,
                    start_layer=args["start_layer"],nb_fc_layer=args["nb_fc_layer"])

#using Jiminy pour compiler
DL= Jiminy(data,model)
DL.Compile()
input('appuyer sur enter pour continuer (pas Ctr+C ): ')
DL.Train(batch_size=args["batch_size"],nb_epoch=args["nb_epochs"],validation_freq=1)
DL.PlotRes()


