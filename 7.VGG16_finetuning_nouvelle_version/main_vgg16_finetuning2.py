# -*- coding: utf-8 -*-

"""
usage exemplaire: python main.py --start_layer -4 --batch_size 1000 --nb_epochs 50 --if_save_model False 
 --nb_fc_layer1 256 --nb_fc_layer2 256 --output output 
vgg16 reconfigure pour la dataset cifar10 :dont les quelques dernires couches trainables (au choix)
Created on Mon Mar 27 11:22:47 2020
@author: lshengkai

"""
#import les libs necessaires
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.optimizers import SGD
import argparse 
#import ma lib 
from function_annexe.net.VGG16_reconfig import VGG16_reconfig
from function_annexe.net.VGG16_reconfig import preprocessing
from function_annexe.callbacks.trainmonitor import TrainingMonitor
import os

# construct the argument parse and parse the arguments(start_layer,batch_size,nb_epochs etc)
def boolean_string(s):
    if s not in {'False', 'True'}:
        raise ValueError('Not a valid boolean string')
    return s == 'True'

ap = argparse.ArgumentParser()
ap.add_argument("-s", "--start_layer", type=int,default=-4,
	help="le premier couche a partir duquel sont trainables")
ap.add_argument("-b", "--batch_size", type=int,default=100,
	help="# la taille de batch dataset")
ap.add_argument("-n", "--nb_epochs", type=int, default=50,
	help="# le nombre de epochs lors d'un apprentissage")
ap.add_argument("-if","--if_save_model",type=boolean_string,required=True,
    help="# si on doit sauvegqrder le model")
ap.add_argument("-nb_fc1","--nb_fc_layer1",type=int,default=1024,required=True,
                help="the number of the ")
ap.add_argument("-nb_fc2","--nb_fc_layer2",type=int,default=1024,required=True,
                help="the number of the ")
ap.add_argument("-o","--output",required=True,help="out put path")
args = vars(ap.parse_args())

#verifier tous les parametres d'entrees
print(args)

#load data 
print("[info] loading cifar-10 data...")
(x_train,y_train),(x_test,y_test)=cifar10.load_data()

#scale les data vers [0,1] et convert label de int vers vect
x_train,y_train,x_test,y_test=preprocessing(x_train,y_train,x_test,y_test)

#load model reconfiguree une image de cifar10(10 classes en total) procede une taille de 32*32*3
print("[info] build model...")
model=VGG16_reconfig.build(width=32,height=32,depth=3,classes=10,
 start_layer=args["start_layer"],nb_fc_layer1=args["nb_fc_layer1"],nb_fc_layer2=args["nb_fc_layer2"])

#model summary
model.summary()

#compile model
print("[info] compile modele...")
opt=SGD(lr=0.01,momentum=0.9,nesterov=True)
model.compile(loss="categorical_crossentropy",optimizer=opt,metrics=["accuracy"])

#construct the set of callbacks
figPath=os.path.sep.join([args["output"],"start_layer={} batch_size={} nb_fc_layer1={}nb_fc_layer2={} nb_epochs={}.png"
                          .format(args["start_layer"],args["batch_size"],args["nb_fc_layer1"],args["nb_fc_layer2"],
                                  args["nb_epochs"])])

jsonPath=os.path.sep.join([args["output"],"start_layer={} batch_size={} nb_fc_layer1={} nb_fc_layer2={} nb_epochs={}.json"
                          .format(args["start_layer"],args["batch_size"],args["nb_fc_layer1"],args["nb_fc_layer2"],args["nb_epochs"])])
callbacks=[TrainingMonitor(figPath,jsonPath=jsonPath)]

#traning 
print("[info] training model...")
H=model.fit(x_train,y_train,validation_data=(x_test,y_test),
            callbacks=callbacks,batch_size=args["batch_size"],epochs=args["nb_epochs"],verbose=1)

#save modele
if(args["if_save_model"]):
 print("[info] serializing model...")
 saufgarder_a='C://Users//lshengkai//Desktop//project_internship//7.VGG16_finetuning//model//'
 model.save(saufgarder_a+"VGG16_reconfig_weights start_layer={} batch_size={} nb_fc_layer1={}nb_fc_layer2={} nb_epochs={}.hdf5"
            .format(args["start_layer"],args["batch_size"],args["nb_fc_layer1"],args["nb_fc_layer2"],args["nb_epochs"]))





