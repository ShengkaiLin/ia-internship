# -*- coding: utf-8 -*-
#import lib 
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D,BatchNormalization,MaxPooling2D,Flatten,Dropout,Dense
from tensorflow.keras import backend as K

class MiniVGGNet:
    @staticmethod
    def build(width,height,depth,classes):
        #initialize the model along with the input shape 
        model=Sequential()
        inputShape=(height,width,depth)
        chanDim=-1
        
        #channels first
        if K.image_data_format()=="channels_first":
            inputShape=(depth,height,width)
            chanDim=1
            
        #frist CONV=>RELU=>CONV=>RELU=>POOL layer set 
        model.add(Conv2D(32,(3,3),padding="same",input_shape=inputShape,
                             activation="relu"))
        model.add(BatchNormalization(axis=chanDim))
        model.add(Conv2D(32,(3,3),padding="same",activation="relu"))
        model.add(BatchNormalization(axis=chanDim))
        model.add(MaxPooling2D(pool_size=(2,2)))
        model.add(Dropout(0.25))
        
        #second CONV=>RELU=>CONV=>RELU=>POOL layer set 
        model.add(Conv2D(64,(3,3),padding="same",input_shape=inputShape,
                             activation="relu"))
        model.add(BatchNormalization(axis=chanDim))
        model.add(Conv2D(64,(3,3),padding="same",activation="relu"))
        model.add(BatchNormalization(axis=chanDim))
        model.add(MaxPooling2D(pool_size=(2,2)))
        model.add(Dropout(0.25))
        
        #only layer of FC=>RELU layers
        model.add(Flatten())
        model.add(Dense(512,activation="relu"))
        model.add(BatchNormalization())
        model.add(Dropout(0.5))
        
        #softmax classifier 
        model.add(Dense(classes,activation="softmax"))
        
        #return 
        return model
        

        
            
