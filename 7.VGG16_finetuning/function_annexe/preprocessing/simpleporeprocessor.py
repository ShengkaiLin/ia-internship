# -*- coding: utf-8 -*-
#import library 
import cv2

class SimplePreprocessor:
    def __init__(self,width,height,inter=cv2.INTER_AREA):
        """
        Parameters
        ----------
        width : int
            premiere dimension d'une image 
        height : int
            deuxieme dimension d'une image 
        inter : methode d'interpolation 
            DESCRIPTION. The default is cv2.INTER_AREA.

        Returns
        -------
        None.

        """
        self.width=width
        self.height=height
        self.inter=inter
     
    def preprocess(self,image):
        """
        Parameters
        ----------
        image : Numpy array
            RGB image 

        Returns
        -------
        une image retaille  

        """
        return cv2.resize(image,(self.width,self.height),interpolation=self.inter)
    
if __name__=="__main__":
    # in mac pyder use run->Configuration per file->run in an xeternal system console
    sp=SimplePreprocessor(128,128)
    image=cv2.imread("/Users/shengkailin/project_intership_2020/datasets/animals/dogs/dogs_00001.jpg")
    image2=sp.preprocess(image)
    cv2.imshow("ImageWindow",image2)
    cv2.waitKey(0) # close window when a key press is detected
    cv2.destroyWindow('image')
    cv2.waitKey(1)


    
     
    